import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'

import store from '@/store/index'

import {checkAccessToRoute} from '@/utils/index'

import {Steps} from '@/models/types'

const HomeView = () => import(/* webpackChunkName: 'route-home' */ '@/views/HomeView.vue')
const ContactInfoView = () => import(/* webpackChunkName: 'route-forms' */ '@/views/forms/ContactInfoView.vue')
const MembershipView = () => import(/* webpackChunkName: 'route-forms' */ '@/views/forms/MembershipView.vue')
const OverviewView = () => import(/* webpackChunkName: 'route-forms' */ '@/views/forms/OverviewView.vue')


const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'home',
        component: HomeView,
        redirect: to => {
            const {query} = to
            return {name: 'contact-info', query}
        },
        children: [{
            path: 'contact-info',
            name: 'contact-info',
            components: {
                'form': ContactInfoView,
            },
        }, {
            path: 'membership',
            name: 'membership',
            beforeEnter: (to, _, next) => checkAccessToRoute(next, Steps.Membership),
            components: {
                'form': MembershipView,
            },
        }, {
            path: 'overview',
            name: 'overview',

            beforeEnter: (to, _, next) => checkAccessToRoute(next, Steps.Overview),
            components: {
                'form': OverviewView,
            },
        }],
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router

import axios, {AxiosPromise} from "axios";
import {UserFormDetails} from "@/models/types";
export default {
    store( data: UserFormDetails ):AxiosPromise {
        return axios.post('https://httpbin.org/post', data);
    }
}

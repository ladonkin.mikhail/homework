import {createStore} from 'vuex'
import {ContactInfo, SelectedPhoneInfo, Membership, Steps, UserFormDetails} from "@/models/types";
import {preselectedPhone} from "@/utils/index"

export default createStore({
    state: {
        formData: {
            firstName: '',
            lastName: '',
            email: '',
            phones: [preselectedPhone] as SelectedPhoneInfo[],
            membership: Membership[Membership.Regular],
        } as UserFormDetails,
        currentStep: Steps.ContactInfo,
    },
    getters: {
        currentStep(state) {
            return state.currentStep
        },
        contactInfo(state) {
            return {
                firstName: state.formData.firstName,
                lastName: state.formData.lastName,
                email: state.formData.email,
                phones: state.formData.phones,
            }
        },
        membership(state) {
            return state.formData.membership
        },
        allInfo(state) {
            return {
                firstName: state.formData.firstName,
                lastName: state.formData.lastName,
                email: state.formData.email,
                phones: state.formData.phones,
                membership: state.formData.membership,
            }
        }
    },
    mutations: {
        SAVE_CONTACT_INFO(state, contactInfo: ContactInfo) {
            state.formData.firstName = contactInfo.firstName
            state.formData.lastName = contactInfo.lastName
            state.formData.email = contactInfo.email
            state.formData.phones = [...contactInfo.phones]
            state.currentStep = Steps.Membership
        },
        SAVE_MEMBERSHIP(state, membership: string) {
            state.formData.membership = membership
            state.currentStep = Steps.Overview
        },
        STEP_BACK(state, step: keyof typeof Steps) {
            state.currentStep = Steps[step]
        },
        EDIT_ALL_INFO(state, payload) {
            state.formData.firstName = payload.firstName
            state.formData.lastName = payload.lastName
            state.formData.email = payload.email
            state.formData.phones = [...payload.phones]
            state.formData.membership = payload.membership
        },
        RESET(state) {
            state.formData.firstName = '';
            state.formData.lastName = '';
            state.formData.email = '';
            state.formData.phones = [preselectedPhone] as SelectedPhoneInfo[];
            state.formData.membership = Membership[Membership.Regular];
            state.currentStep = Steps.ContactInfo;
        }
    },
    actions: {
        saveContactInfo({commit}, payload: ContactInfo) {
            commit('SAVE_CONTACT_INFO', payload)
        },
        saveMembership({commit}, payload: Membership) {
            commit('SAVE_MEMBERSHIP', payload)
        },
        editAllInfo({commit}, payload) {
            commit('EDIT_ALL_INFO', payload)
        },
        reset({commit}) {
            commit('RESET')
        },
        stepBack({commit}, step: keyof typeof Steps) {
            commit('STEP_BACK', step)
        }
    },
    modules: {}
})

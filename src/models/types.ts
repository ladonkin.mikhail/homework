export type ContactInfo = {
    firstName: string,
    lastName: string,
    email: string,
    phones: SelectedPhoneInfo[]
}

enum PhoneType {
    'Home',
    'Work',
    'Mobile',
    'Main',
    'Other',
}

export enum Membership {
    'Regular',
    'Premium',
}

export enum Steps {
    'ContactInfo',
    'Membership',
    'Overview',
}

export type PhoneItem = {
    id: number,
    name: keyof typeof PhoneType
}

export type SelectedPhoneInfo = {
    id: number,
    number: string,
    selectedType: PhoneItem,
}

export type ContactInfoErrors = {
    firstName?: boolean,
    lastName?: boolean,
    email?: boolean,
    phones?: boolean,
}

export type UserFormDetails = {
    firstName: string,
    lastName: string,
    email: string,
    membership?: string,
    phones: Record<string, string | number> | SelectedPhoneInfo[]
}

export const stepNames = {
    personalInfo: 'Personal Info',
    membership: 'Membership',
    overview: 'Overview'
}

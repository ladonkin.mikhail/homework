import {NavigationGuardNext} from 'vue-router'
import store from '@/store'
import {SelectedPhoneInfo, PhoneItem, ContactInfoErrors, UserFormDetails} from '@/models/types'

export function checkAccessToRoute(next: NavigationGuardNext, routeStep: number) {
    const currentStep = store.state.currentStep
    if (currentStep < routeStep) {
        next({name: 'contact-info'})
        return
    }
    next()
}

export const preselectedPhone: SelectedPhoneInfo = {
    id: 0,
    number: '',
    selectedType: {
        id: 0,
        name: 'Home',
    },
}

export function handleUserInput(e: Event, phone: SelectedPhoneInfo): void {
    // adds very basic custom phone mask
    const inputTarget = e.target as HTMLInputElement
    const notANumberRegex = /\D/g
    const phonePatternRegex = /(\d{0,3})(\d{0,3})(\d{0,4})/
    const replacedInput = inputTarget.value.replace(notANumberRegex, '').match(phonePatternRegex);
    if (replacedInput) phone.number = !replacedInput[2] ? replacedInput[1] : `(${replacedInput[1]}) ${replacedInput[2]} ${replacedInput[3] ? replacedInput[3] : ''}`;
}

export const allPhoneOptions = [
    {
        id: 0,
        name: 'Home',
    },
    {
        id: 1,
        name: 'Work',
    },
    {
        id: 2,
        name: 'Mobile',
    },
    {
        id: 3,
        name: 'Main',
    },
    {
        id: 4,
        name: 'Other',
    }
] as ReadonlyArray<PhoneItem>

export function validate(errors: ContactInfoErrors, form: UserFormDetails): boolean {
    // adds very basic validation rules
    const validEmailRegex = /^\S+@\S+\.\S+$/
    errors.email = form.email?.length < 1 || !(validEmailRegex.test(form.email))
    errors.firstName = form.firstName?.length < 1
    errors.lastName = form.lastName?.length < 1
    if (Array.isArray(form.phones)) {
        errors.phones = form.phones?.some((phone: SelectedPhoneInfo) => phone.number.length < 5)
    }
    return !Object.values(errors).some(Boolean);
}
